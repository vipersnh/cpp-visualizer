# Package

version       = "0.1.0"
author        = "vipersnh"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["cpp_visualizer"]


# Dependencies

requires "nim >= 0.19.4"
